# DS4 BDADDR
A small utility to read/write the bluetooth device address stored in a dualshock 4 controller.
This is useful for getting the bluetooth mac address of a PS4 or for pairing it with an ESP32.
