use std::{process::exit, time::Duration};
const SONY_VID: u16 = 0x054C;

fn main() -> rusb::Result<()> {
    let mut args = std::env::args();
    let _program = args.next();
    let bdaddr = args.next();

	let mut found = false;
    for device in rusb::devices().unwrap().iter() {
        let device_desc = device.device_descriptor().unwrap();
        if device_desc.vendor_id() != SONY_VID {
            continue;
        }
        found = true;
        let mut handle = device.open()?;
        if handle.kernel_driver_active(0)? {
            handle.detach_kernel_driver(0)?;
        }
        handle.claim_interface(0)?;
        match bdaddr {
            Some(addr) => {
                let addr_bytes: Vec<u8> = addr
                    .split(':')
                    .map(|hex| match u8::from_str_radix(hex, 16) {
                        Ok(byte) => byte,
                        Err(e) => {
                            eprintln!("{e}");
                            exit(1);
                        }
                    })
                    .rev()
                    .collect();
                if addr_bytes.len() != 6 {
                    eprintln!(
                        "A mac address is 6 bytes but the address given is {} bytes",
                        addr_bytes.len()
                    );
                    exit(1);
                }
                handle.write_control(
                    0x21,
                    0x09,
                    0x0313,
                    0x0000,
                    &[
                        0x13,
                        addr_bytes[0],
                        addr_bytes[1],
                        addr_bytes[2],
                        addr_bytes[3],
                        addr_bytes[4],
                        addr_bytes[5],
                        0x56,
                        0xE8,
                        0x81,
                        0x38,
                        0x08,
                        0x06,
                        0x51,
                        0x41,
                        0xC0,
                        0x7F,
                        0x12,
                        0xAA,
                        0xD9,
                        0x66,
                        0x3C,
                        0xCE,
                    ],
                    Duration::new(1, 0),
                )?;
            }
            None => {
                let mut buffer = [0; 16];
                let len = handle.read_control(
                    0xA1,
                    0x01,
                    0x0312,
                    0x0000,
                    &mut buffer,
                    Duration::new(1, 0),
                )?;
                if len != buffer.len() {
                    eprintln!("Error: Partial read of packet");
                    exit(1);
                }
                for i in 0..6 {
                    print!("{:02x}", buffer[15 - i]);
                    if i < 5 {
                        print!(":");
                    }
                }
                println!("");
            }
        }
        break;
    }
    if !found {
    	eprintln!("No device found");
    	exit(1);
    }
    Ok(())
}
